# Geology Data Utils v1.1.0

## Getting Started

install from pypi

```
pip install geo-data-utils
```

Install direct from git

```
pip install git+https://gitlab.com/csiro-geoanalytics/python-shared/geo-data-utils.git
```

Or clone the repo so you can make changes or view the code, note the -e in the pip install is optional, it makes it so any code changes are automatically picked up.

```
git clone https://gitlab.com/csiro-geoanalytics/python-shared/geo-data-utils.git
<Navigate to the code directory>
pip install -e .
```

If you would like to work on the code in a virtual env a pipfile is available

```
pip install pipenv
pipenv install
pipenv shell
```

## Usage

Once it's installed use the command 'geo-data-utils'

TODO: more info
