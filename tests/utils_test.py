import sys
import os, shutil
import pytest

# from loguru import logger
from geo_utils import filter_data_by_ranges_from_file, down_hole_signal_plots

dirname = os.path.dirname(__file__)
output_dir = os.path.join(dirname, "output")


def rf_dir(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print("Failed to delete %s. Reason: %s" % (file_path, e))


@pytest.fixture
def clean_output():
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    rf_dir(output_dir)


# @pytest.fixture
# def mp_logger():
#     logger.remove()

#     logger.add(
#         sys.stdout,
#         enqueue=True,
#         level="DEBUG",
#         format="<green>{time:HH:mm:ss}</green> | <cyan>{process}</cyan> | <level>{message}</level>",
#     )


def test_filter_range(clean_output):
    output_file = os.path.join(output_dir, "output.xlsx")
    try:
        filter_data_by_ranges_from_file(
            os.path.join(dirname, "data", "data.xlsx"),
            os.path.join(dirname, "data", "filter.xlsx"),
            "from",
            "to",
            "depth",
            output_file,
        )
    except Exception as e:
        print("Exception Caught running test_filter_range")
        pytest.fail(str(e))

    assert os.path.exists(output_file)


def test_down_hole_signal_plots():
    output_file = os.path.join(output_dir, "output.html")
    try:
        down_hole_signal_plots(
            os.path.join(dirname, "data", "test_data.csv"),
            y_column="Sample",
            var_columns=["Fe", "SiO2", "TiO2"],
            output_file=output_file,
            colours=["black", "red", "blue"],
            ignore_rows=[1],
        )
    except Exception as e:
        print("Exception Caught running test_filter_range")
        pytest.fail(str(e))

    assert os.path.exists(output_file)


def test_down_hole_signal_plots_no_vars():
    output_file = os.path.join(output_dir, "output_no_vars.html")
    try:
        down_hole_signal_plots(
            os.path.join(dirname, "data", "test_data.csv"),
            y_column="Sample",
            output_file=output_file,
            ignore_rows=[1],
        )
    except Exception as e:
        print("Exception Caught running test_filter_range")
        pytest.fail(str(e))

    assert os.path.exists(output_file)


def test_down_hole_signal_plots_with_depths():
    output_file = os.path.join(output_dir, "output_depths.html")
    try:
        down_hole_signal_plots(
            os.path.join(dirname, "data", "test_data2.csv"),
            from_column='depth from ',
            to_column='depth to ',
            output_file=output_file,
            ignore_rows=[1],
        )
    except Exception as e:
        print("Exception Caught running test_filter_range")
        pytest.fail(str(e))

    assert os.path.exists(output_file)
